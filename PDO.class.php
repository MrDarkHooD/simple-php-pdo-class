<?php
/* This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details. */

/* Creator: https://gitlab.com/MrDarkHooD */

class Database
{
	private $Host;
	private $DBPort;
	private $DBName;
	private $DBUser;
	private $DBPassword;
	private $DBEngine;
	private $connection;
	private $logPath;

	public function __construct(
		string $Host,
		int $DBPort,
		string $DBName,
		string $DBUser,
		string $DBPassword,
		string $DBEngine = "mysql"
	)
	{
		$this->Host	   = $Host;
		$this->DBPort	 = $DBPort;
		$this->DBName	 = $DBName;
		$this->DBUser	 = $DBUser;
		$this->DBPassword = $DBPassword;
		$this->DBEngine   = $DBEngine;
		$this->logPath	= '/www/logs/pdo/';
		$this->connect();
	}

	private function connect()
	{
		try {
			$this->connection = new PDO
			(
				"$this->DBEngine:host=$this->Host;
				port=$this->DBPort;
				dbname=$this->DBName;",
				$this->DBUser,
				$this->DBPassword,
#				array
#				(
#					PDO::ATTR_PERSISTENT => true,
#					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
#					PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
#					PDO::MYSQL_ATTR_FOUND_ROWS => true
#				)
			);
			$this->connection->setAttribute(
				PDO::ATTR_ERRMODE,
				PDO::ERRMODE_EXCEPTION
			);

		} catch (PDOException $e) {
			self::errorLog( $e );
		}
	}

	public function query (
		$sql,
		$params = [],
		$fetchmode = PDO::FETCH_ASSOC
	): mixed
	{
		$query = self::init (
			$sql,
			$params
		);

		$result = [];
		$statement = strtolower( explode( ' ', $sql )[0] );

		if ( in_array( $statement, ['select', 'show'] ) )
		{
			while ( $line = $query->fetch( $fetchmode ) )
			{
				array_push( $result, $line );
			}
		}
		elseif ( in_array( $statement, ['update', 'delete'] ) )
		{
			$result = $query->rowCount();
		}
		elseif ( $statement == 'insert' )
		{
			$result = $this->connection->lastInsertId();
		}
		$query->closeCursor();

		return $result;
	}

	public function single(
		$sql,
		$params = [],
		$fetchmode = 0
	): mixed
	{
		$query = self::init(
			$sql,
			$params
		);

		$result = $query->fetchColumn( $fetchmode );
		return $result;
	}

	public function row(
		$sql,
		$params = [],
		$fetchmode = PDO::FETCH_ASSOC
	): array
	{
		$query = self::init(
			$sql,
			$params
		);

		$result = $query->fetch( $fetchmode );
		$query->closeCursor();

		if ( is_bool( $result ) )
		{
			$result = array();
		}

		return $result;
	}

	public function column
	(
		$sql,
		$params = [],
		$fetchmode = PDO::FETCH_COLUMN
	): array
	{
		$query = self::init(
			$sql,
			$params
		);
		$result = $query->fetchAll($fetchmode);
		$query->closeCursor();
		
		return $result;
	}

	public function insert
	(
		$table,
		$columns,
	): int
	{
		if ( $this->DBEngine == "mysql" )
		{
			$sql = "INSERT INTO $table ";
			$sql .= "(`" . implode( '`,`', $columns ) . "`) VALUES (?";

			$sql .= ",?" . str_repeat(", ?", count( $columns ));

			$sql .= ")";
		}
		elseif ( $this->DBEngine == "pgsql" )
		{
			$sql = "INSERT INTO $table ";
			$sql .= "(" . implode( ', ', array_keys($columns) ) . ") VALUES (:";

			$sql .= implode( ', :', array_keys($columns) );

			$sql .= ")";
		}

		self::init(
			$sql,
			$columns
		);

		return $this->connection->lastInsertId();
	}

	public function update
	(
		$table,
		$columns,
		$conditions,
		$debug = false
	): mixed
	{

		// This loop will prevent collision
		foreach ($conditions as $key => $value) {
			$i = null;
			while (array_key_exists($key.$i, $columns)) {
				$i++;
			}
			unset($conditions[$key]);
			$conditions[$key.$i] = $value;
		}
		
		$sql = "UPDATE\n\t$table\nSET\n";
		foreach( $columns as $cname => $value )
		{
			$sql .= "\t$cname = :$cname";
			if ($cname !== array_key_last($columns))
			{
				$sql .= ",";
			}
			$sql .= "\n";
		}

		$sql .= "WHERE\n";
		foreach($conditions as $cname => $value)
		{
			
			$sql .= "\t";
			if (
				$cname !== array_key_first($conditions) &&
				count($conditions) > 1
			
			)
			{
				$sql .= "AND ";
			}
			
			$sql .= "$cname = :$cname";
			if ($cname !== array_key_last($conditions))
			{
				$sql .= "\n";
			}
		}

		$values = array_merge($columns, $conditions);
		
		$status = self::init(
			$sql,
			$values
		);

		return $status;
	}

	public function lastInsertId (): int
	{
		return $this->connection->lastInsertId();
	}

	public function rollBack()
	{
		return $this->connection->rollBack();
	}

	public function beginTransaction()
	{
		return $this->connection->beginTransaction();
	}

	public function inTransaction()
	{
		return $this->connection->inTransaction();
	}

	public function commit()
	{
		return $this->connection->commit();
	}

	private function init(
		$sql,
		$params
	)
	{
		if ( $this->DBEngine == "pgsql" )
		{
			foreach ( $params as $key => $param )
			{
				if ( gettype( $param ) == "boolean" )
				{
					$params[$key] = ($param) ? "true" : "false";
				}
			}
		}
		
		try {
			$query = $this->connection->prepare( $sql );
			$query->execute( $params );
			return $query;
		}
		catch ( PDOException $e )
		{
			self::errorLog( $e );
		}
	}

	private function errorLog ( $error ): void
	{
		$file = $this->logPath . date("Y-m-d") . '.txt';
		file_put_contents(
			$file,
			date("H:i:s") . '\n' . $error . '\n\n',
			FILE_APPEND | LOCK_EX
		);
		echo $error;
		die('ERROR: Check log for more information');
	}

}