# Simple PHP PDO class

PHP class to use sql databases with short oneline commands.  
Atleast read source before you do. There is atleast path for logging you need to change.  

## Usage
```php
<?php
//Make new connection, DBEngine is optional, default is mysql
$DB = new DB(DBHost, DBName, DBUser, DBPassword, DBEngine);

//Do normal query, output is multidimensional array if select or show, count of affected rows if update or delete.
$query = $DB->query("SELECT * FROM messages WHERE deleted = ? LIMIT ?", array(0, 10));
var_dump (
    array (
        0 => array (
            "first" => "firstvalue",
            "second" => "secondvalue"
        )
        1 => array (
            "first" => "firstvalue",
            "second" => "secondvalue"
        )
    )
)

//Do single result query, output is string
$query = $DB->single("SELECT username FROM message WHERE id = ?", [9001]);
var_dump("value");

// Do query for 1 row, output is single dimensional array
$query = $DB->row("SELECT * FROM message WHERE id = ?", [9001]);
var_dump (
    array (
        "first" => "firstvalue",
        "second" => "secondvalue",
    )
)

// Do query for column, output is single dimensional array
$query = $DB->column("SELECT time FROM message");
var_dump (
    array (
        1711415435,
        1711415436,
        1711415437,
        1711415438
    )
)

// Insert data in database, returns id of inserted column as int
// MySQL
$DB->insert("table_name", ["column1", "column2"], ["data_to_column_1", "data_to_column_2"]);

// Postgres
$DB->insert(
    "table_name",
    [
        "column1" => "data_to_column_1",
        "column2" => "data_to_column_2"
    ]
);

// Update table in database, returns something idk
$DB->update(
    "table_name",
    [
        "column1" => "new_value_to_column_1",
        "column2" => "new_value_to_column_2"
    ],
    [
        "where_this_column_is" => "this value",
        "and_another_column_is" => "that value"
    ]
);
?>
```
